package summative.one;

public class TrafficLight {
    public static final String RED = "RED";
    public static final String ORANGE = "ORANGE";
    public static final String GREEN = "GREEN";

    private String name;
    private String light = RED;
    private String nextCycleLight = GREEN;

    public TrafficLight(String name) {
        this.name = name;
    }

    public void cycle() {
        try {
            if (light.equals(RED)) {
                nextCycleLight = GREEN;
                light = ORANGE;
            } else if (light.equals(ORANGE)) {
                light = nextCycleLight;
            } else {
                nextCycleLight = RED;
                light = ORANGE;
            }
        } catch (Exception e) {
        }
    }

    public String announce(String formattedTime) {
        return String.format("Light %s turns %s at %s\n", getName(), light, formattedTime);
    }

    public String getName() {
        return name;
    }

    public String getLight() {
        return light;
    }

}
