package summative.one;

import java.io.FileWriter;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class TrafficController {

    private LocalTime currentTime = LocalTime.of(13, 0);

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    private int currentIndex = 0;
    private ArrayList<TrafficLight> lights = new ArrayList<>();

    public void startCycles() {

        currentTime.plusHours(3);

        TrafficLight tl4 = new TrafficLight("TL-04");
        TrafficLight tl3 = new TrafficLight("TL-03");
        TrafficLight tl2 = new TrafficLight("TL-02");
        TrafficLight tl1 = new TrafficLight("TL-01");

        lights.add(tl1);
        lights.add(tl2);
        lights.add(tl3);
        lights.add(tl4);

        try {
            FileWriter fileWriter = new FileWriter("traffic-light1.txt.txt");
            for (int i = 0; i < 100; i++) {
                TrafficLight currentLight = lights.get(currentIndex);
                if (currentLight.getLight().equals(TrafficLight.ORANGE)) {
                    currentTime = currentTime.plusSeconds(120);
                } else {
                    currentTime = currentTime.plusSeconds(5);
                }
                currentLight.cycle();
                fileWriter.write(
                        currentLight.announce(currentTime.format(formatter)));

                if (currentLight.getLight().equals(TrafficLight.GREEN)) {
                    fileWriter.write(
                            announcePedestrians(lights.get(currentIndex + 1 >= lights.size() ? 0 : currentIndex + 1)));
                }
                if (currentLight.getLight().equals(TrafficLight.RED)) {
                    setNextIndex();
                }
            }
            fileWriter.close();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    private void setNextIndex() {
        if (currentIndex + 1 >= lights.size()) {
            currentIndex = 0;
        } else {
            currentIndex++;
        }
    }

    private String announcePedestrians(TrafficLight trafficLight) {
        return String.format("Pedestrians at light %s can cross\n", trafficLight.getName());
    }

}
