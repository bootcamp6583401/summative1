package summative.four;

import java.util.ArrayList;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {

        System.out.println("EXAMPLE 1");
        ArrayList<Customer> waitingCustomers = new ArrayList<>();

        waitingCustomers.add(new Customer("Jim Halpert", 2, 0));
        waitingCustomers.add(new Customer("Kevin Malone", 4, 0));
        waitingCustomers.add(new Customer("Pam Beesley", 4, 2));

        Elevator elevator = new Elevator(4, waitingCustomers);

        elevator.autoPilot();

        System.out.println("\nEXAMPLE 2! Reseting...");

        elevator.moveTo(0);
        waitingCustomers.clear();

        waitingCustomers.add(new Customer("Jim Halpert", 0, 2));
        waitingCustomers.add(new Customer("Kevin Malone", 0, 4));

        elevator.setWaitingCustomers(waitingCustomers);

        System.out.println("Reset complete!");
        elevator.autoPilot();

        System.out.println("\nEXAMPLE 3! Reseting...");

        elevator.moveTo(3);
        waitingCustomers.clear();

        waitingCustomers.add(new Customer("Jim Halpert", 1, 4));
        waitingCustomers.add(new Customer("Michael Scott", 4, 2));
        waitingCustomers.add(new Customer("Kevin Malone", 4, 1));

        elevator.setWaitingCustomers(waitingCustomers);

        System.out.println("Reset complete!");
        elevator.autoPilot();

        // System.out.println(elevator.getShortestLevelPath());

    }
}
