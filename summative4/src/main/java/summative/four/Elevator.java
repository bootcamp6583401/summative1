package summative.four;

import java.util.ArrayList;
import java.util.Iterator;

public class Elevator {
    private int currentLevel;

    private ArrayList<Customer> containedCustomers = new ArrayList<>();
    private ArrayList<Customer> waitingCustomers = new ArrayList<>();

    public Elevator(int startingLevel, ArrayList<Customer> waitingCustomers) {
        this.currentLevel = startingLevel;
        this.waitingCustomers = waitingCustomers;
    }

    public void moveUp() {
        if (currentLevel < 4) {
            currentLevel++;
            System.out.println("Elevator is moving up to level " + getLevelName(currentLevel));
        } else {
            System.out.println("Cannot move up.");
        }
    }

    public void moveDown() {
        if (currentLevel > 0) {
            currentLevel--;
            System.out.println("Elevator is moving down to level " + getLevelName(currentLevel));
        } else {
            System.out.println("Cannot move down.");
        }
    }

    public void openDoors() {
        System.out.println("Doors are opening at level " + getLevelName(currentLevel));
    }

    public void closeDoors() {
        System.out.println("Doors are closing.");
    }

    public static String getLevelName(int currentLevel) {
        switch (currentLevel) {
            case 0:
                return "Basement";
            case 1:
                return "Ground Floor";
            case 2:
                return "1st Floor";
            case 3:
                return "2nd Floor";
            case 4:
                return "3rd Floor";
            default:
                return "Unknown Level";
        }
    }

    public int getCurrentLevel() {
        return currentLevel;
    }

    public void addCustomer(Customer customer) {
        this.containedCustomers.add(customer);
    }

    public void dropCustomers() {

        Iterator<Customer> iterator = containedCustomers.iterator();
        while (iterator.hasNext()) {
            Customer customer = iterator.next();
            if (customer.getDestinationFloor() == currentLevel) {
                iterator.remove();
                System.out.println(
                        "Customer " + customer.getName() + " has reached their destination and left the elevator at "
                                + getLevelName(currentLevel));
            }
        }
    }

    public void pickUpCustomers() {

        Iterator<Customer> iterator = waitingCustomers.iterator();
        while (iterator.hasNext()) {
            Customer customer = iterator.next();
            if (customer.getWaitingFloor() == currentLevel) {
                containedCustomers.add(customer);
                iterator.remove();
                System.out.println(
                        "Customer " + customer.getName() + " has entered the elevator at "
                                + getLevelName(currentLevel));
            }
        }

    }

    public void autoPilot() {
        while (waitingCustomers.size() > 0 || containedCustomers.size() > 0) {
            int nextLevel = getShortestLevelPath();
            // System.out.println("Target: " + getLevelName(nextLevel));
            while (nextLevel != currentLevel) {
                if (nextLevel > currentLevel) {
                    moveUp();
                } else {
                    moveDown();
                }
            }
            // System.out.println("Currently at level " + getLevelName(currentLevel));
            dropCustomers();
            pickUpCustomers();

            // System.out.println("Waiting customers: " + waitingCustomers.size());
            // System.out.println("Contained customers: " + containedCustomers.size());
        }
    }

    public void moveTo(int targetLevel) {
        while (targetLevel != currentLevel) {
            if (targetLevel > currentLevel) {
                moveUp();
            } else {
                moveDown();
            }
        }
    }

    public int getShortestLevelPath() {
        ArrayList<Integer> validLevels = new ArrayList<>();
        for (Customer customer : waitingCustomers) {
            if (!validLevels.contains(customer.getWaitingFloor())) {
                validLevels.add(customer.getWaitingFloor());
            }
        }

        for (Customer customer : containedCustomers) {
            if (!validLevels.contains(customer.getDestinationFloor())) {
                validLevels.add(customer.getDestinationFloor());
            }
        }

        int shortestLevel = -100;

        for (int level : validLevels) {
            // System.out.println(Math.abs(currentLevel - level));
            if (Math.abs(currentLevel - level) <= Math.abs(currentLevel - shortestLevel)) {

                shortestLevel = level;
                // System.out.println("New shortest Level: " + shortestLevel);
            }
        }
        return shortestLevel;
    }

    public void setWaitingCustomers(ArrayList<Customer> customers) {
        this.waitingCustomers = customers;
    }

}
