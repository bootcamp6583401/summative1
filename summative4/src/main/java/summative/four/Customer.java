package summative.four;

public class Customer {
    private String name;
    private int destinationFloor;
    private int waitingFloor;

    public Customer(String name, int destinationFloor, int waitingFloor) {
        this.name = name;
        if (destinationFloor >= 0 && destinationFloor <= 4) {
            this.destinationFloor = destinationFloor;
        } else {
            this.destinationFloor = 0;
        }

        this.waitingFloor = waitingFloor;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDestinationFloor() {
        return destinationFloor;
    }

    public void setDestinationFloor(int destinationFloor) {
        this.destinationFloor = destinationFloor;
    }

    public int getWaitingFloor() {
        return this.waitingFloor;
    }
}
