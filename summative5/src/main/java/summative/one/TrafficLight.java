package summative.one;

import java.util.Random;

public class TrafficLight {
    public static final String RED = "RED";
    public static final String ORANGE = "ORANGE";
    public static final String GREEN = "GREEN";

    private String name;
    private String light = RED;
    private String nextCycleLight = GREEN;
    private int density;

    private boolean takeFromNextGreen = false;

    private Random random = new Random();

    public TrafficLight(String name) {
        this.name = name;
        this.density = random.nextInt(200) + 1;

        System.out.println(String.format("%s starts with %d vehicles", name, density));
    }

    public void cycle() {
        try {
            if (light.equals(RED)) {
                nextCycleLight = GREEN;
                light = ORANGE;
            } else if (light.equals(ORANGE)) {
                light = nextCycleLight;
            } else {
                nextCycleLight = RED;
                light = ORANGE;
            }
        } catch (Exception e) {
        }
    }

    public String announce(String formattedTime) {
        return String.format("Light %s turns %s at %s\n", getName(), light, formattedTime);
    }

    public String getName() {
        return name;
    }

    public String getLight() {
        return light;
    }

    public void setDensity() {
        this.density = random.nextInt(200) + 1;
    }

    public int getDensity() {
        return density;
    }

    public String getNextCycleLight() {
        return nextCycleLight;
    }

    public boolean getShouldTake() {
        return takeFromNextGreen;
    }

    public void setShouldTake(boolean value) {
        this.takeFromNextGreen = value;
    }

}
