package summative.one;

import java.io.FileWriter;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class TrafficController {

    private LocalTime currentTime = LocalTime.of(13, 0);

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    private int currentIndex = 0;
    private ArrayList<TrafficLight> lights = new ArrayList<>();

    public void startCycles() {

        currentTime.plusHours(3);

        TrafficLight tl4 = new TrafficLight("TL-04");
        TrafficLight tl3 = new TrafficLight("TL-03");
        TrafficLight tl2 = new TrafficLight("TL-02");
        TrafficLight tl1 = new TrafficLight("TL-01");

        lights.add(tl1);
        lights.add(tl2);
        lights.add(tl3);
        lights.add(tl4);

        try {
            FileWriter fileWriter = new FileWriter("smart-traffic-light2.txt");
            for (int i = 0; i < 2000; i++) {
                TrafficLight currentLight = lights.get(currentIndex);

                // check density
                boolean densityCheckRes = TrafficController.passDensityCheck(currentLight, lights);

                if (currentLight.getLight().equals(TrafficLight.ORANGE)) {
                    currentTime = currentTime.plusSeconds(120);
                    if (densityCheckRes && currentLight.getNextCycleLight().equals(TrafficLight.GREEN)) {
                        fileWriter.write(
                                "Traffic Light " + currentLight.getName() + " is super dense adding 30 seconds!\n");
                        currentTime = currentTime.plusSeconds(30);
                        getLightWithLowestDensity(lights, currentLight).setShouldTake(true);
                    }

                    if (currentLight.getShouldTake()) {
                        currentTime = currentTime.minusSeconds(30);
                        fileWriter.write("Traffic Light " + currentLight.getName() + " is getting subtracted!\n");
                        currentLight.setShouldTake(false);
                    }
                } else {
                    currentTime = currentTime.plusSeconds(5);
                }
                currentLight.cycle();
                fileWriter.write(
                        currentLight.announce(currentTime.format(formatter)));

                if (currentLight.getLight().equals(TrafficLight.GREEN)) {
                    fileWriter.write(
                            announcePedestrians(lights.get(currentIndex + 1 >= lights.size() ? 0 : currentIndex + 1)));
                }
                if (currentLight.getLight().equals(TrafficLight.RED)) {
                    currentLight.setDensity();
                    fileWriter.write("Traffic Light " + currentLight.getName() + " has " + currentLight.getDensity()
                            + " density!\n");
                    setNextIndex();
                }
            }
            fileWriter.close();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    private void setNextIndex() {
        if (currentIndex + 1 >= lights.size()) {
            currentIndex = 0;
        } else {
            currentIndex++;
        }
    }

    private String announcePedestrians(TrafficLight trafficLight) {
        return String.format("Pedestrians at light %s can cross\n", trafficLight.getName());
    }

    public static boolean passDensityCheck(TrafficLight lightToCheck, ArrayList<TrafficLight> allLights) {
        int totalDensity = 0;

        for (TrafficLight tl : allLights) {
            if (tl != lightToCheck) {
                totalDensity += tl.getDensity();
            }
        }

        return lightToCheck.getDensity() > 1.3 * totalDensity;

    }

    public static TrafficLight getLightWithLowestDensity(ArrayList<TrafficLight> lights, TrafficLight lightToExclude) {
        TrafficLight lowest = null;
        for (TrafficLight tl : lights) {
            if (lowest == null || lowest.getDensity() > tl.getDensity() && tl != lightToExclude) {
                lowest = tl;
            }
        }
        return lowest;
    }

}
