package summative.two;

import java.time.Duration;
import java.time.format.DateTimeFormatter;

public class ParkingSystem {

    public static final String TYPE_CAR = "TYPE_CAR";
    public static final String TYPE_MOTORCYCLE = "TYPE_MOTORCYCLE";

    public static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    static public int calculateTotal(Parking parking) {
        int basePrice = parking.getType().equals(ParkingSystem.TYPE_CAR) ? 4500 : 3500;
        return calculateTotalHours(parking) * basePrice + 500;
    }

    static public int calculateTotalHours(Parking parking) {
        Duration exclusiveDuration = Duration.between(parking.getParkTime(), parking.getLeaveTime());
        double hours = (double) exclusiveDuration.toMinutes() / 60;
        return (int) Math.ceil(hours);
    }

    static public void announceTotal(Parking parking) {
        System.out.println(
                String.format("---%s---\nEnter Time: %s\nExit Time: %s\nTotal Duration: %d hours\nTotal Payment: %s\n",
                        parking.getType(),
                        parking.getParkTime().format(formatter),
                        parking.getLeaveTime().format(formatter),
                        calculateTotalHours(parking),
                        calculateTotal(parking)

                ));
    }
}
