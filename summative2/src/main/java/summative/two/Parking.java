package summative.two;

import java.time.LocalDateTime;

public class Parking {

    private LocalDateTime parkTime;
    private LocalDateTime leaveTime;
    private String type;

    public Parking(LocalDateTime parkTime, LocalDateTime leaveTime, String type) {
        this.parkTime = parkTime;
        this.leaveTime = leaveTime;
        this.type = type;
    }

    public LocalDateTime getParkTime() {
        return parkTime;
    }

    public LocalDateTime getLeaveTime() {
        return leaveTime;
    }

    public String getType() {
        return type;
    }

}
