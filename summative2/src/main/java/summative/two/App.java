package summative.two;

import java.time.LocalDateTime;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        // 30 minutes
        ParkingSystem.announceTotal(
                new Parking(LocalDateTime.of(2024, 1, 1, 10, 30), LocalDateTime.of(2024, 1, 1, 11, 0),
                        ParkingSystem.TYPE_CAR));
        // 1 hour 2 minutes
        ParkingSystem.announceTotal(
                new Parking(LocalDateTime.of(2024, 1, 1, 10, 30), LocalDateTime.of(2024, 1, 1, 11, 32),
                        ParkingSystem.TYPE_CAR));
        // 4 hours
        ParkingSystem.announceTotal(
                new Parking(LocalDateTime.of(2024, 1, 1, 10, 30), LocalDateTime.of(2024, 1, 1, 14, 30),
                        ParkingSystem.TYPE_MOTORCYCLE));
        // 6 hours 59 minutes
        ParkingSystem.announceTotal(
                new Parking(LocalDateTime.of(2024, 1, 1, 10, 0), LocalDateTime.of(2024, 1, 1, 16, 59),
                        ParkingSystem.TYPE_CAR));
        // 40 hours
        ParkingSystem.announceTotal(
                new Parking(LocalDateTime.of(2024, 1, 1, 10, 0), LocalDateTime.of(2024, 1, 3, 2, 0),
                        ParkingSystem.TYPE_MOTORCYCLE));
    }
}
