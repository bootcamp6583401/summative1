package summative.three;

public class MockingClass {
    private int privateNum;

    public MockingClass(int privateNum) {
        this.privateNum = privateNum;
    }

    public int doublePrivateNum() {
        return privateNum + 2;
    }
}
