package summative.three;

/**
 * SummativeIntermediate
 */
public class SummativeIntermediate {

    public static void main(String[] args) {
        for (int i = 0; i < args.length; i++) {
            System.out.println(Integer.parseInt(args[i]));
        }
    }

    public static int sum(int a, int b) {
        // Simulate ineffective method
        int total = a;
        for (int i = 1; i <= b; i++) {
            System.out.println(i);
            total++;
        }

        return total;
    }

}
