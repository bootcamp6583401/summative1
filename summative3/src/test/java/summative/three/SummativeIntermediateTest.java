package summative.three;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SummativeIntermediateTest {

    @Test
    public void testMainWithArguments() {
        String[] args = { "1", "2", "3" };

        SummativeIntermediate.main(args);

    }

    @Test
    public void testMainWithNoArguments() {
        String[] args = {};

        SummativeIntermediate.main(args);

    }

    @Test
    public void testSumMethod() {
        int answer = SummativeIntermediate.sum(1, 3);
        assertEquals(answer, 4);
    }

    @Test
    public void testSumMethodWithNegativeArguments1() {
        assertEquals(1, SummativeIntermediate.sum(-1, 2));
    }

    @Test
    public void testSumMethodWithNegativeArguments2() {
        assertEquals(-13, SummativeIntermediate.sum(-11, -2));
    }

    @Test
    public void testSumMethodWithBiggerFirstArgument() {
        assertEquals(12, SummativeIntermediate.sum(10, 2));
    }

    @Test
    public void testSumMethodWithBiggerFirstArgumentNegative() {
        assertEquals(-30, SummativeIntermediate.sum(-10, -20));
    }

    @Test
    public void testSumMethodWithPositiveAndNegative() {
        assertEquals(-30, SummativeIntermediate.sum(10, -20));
    }

    @Test
    public void mockClass1() {
        // Create a mock object
        MockingClass myMock = mock(MockingClass.class);

        when(myMock.doublePrivateNum()).thenReturn(3);

        int result = myMock.doublePrivateNum();

        verify(myMock).doublePrivateNum();

        assertEquals(2, result);
    }

}
